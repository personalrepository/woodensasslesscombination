var gulp = require('gulp');
var sass = require('gulp-sass');
var less = require('gulp-less');
var concat = require('gulp-concat');
var merge = require('merge-stream');


gulp.task('default', function() {  
  gulp.src('./src/style.less')
    .pipe(less())
    .pipe(gulp.dest('assets/dist/'));
});

/*
gulp.task('default', function() {

  var lessStream = gulp.src('./src/style.less')
      .pipe(less())
      .pipe(concat('style.less'))
  ;

  var scssStream = gulp.src('./src/style.sass')
      .pipe(sass())
      .pipe(concat('style.scss'))
  ;
  
  var cssStream = gulp.src('./assets/dist/style.css')
      .pipe(concat('style.css'))
  ;

  var mergedStream = merge(lessStream, scssStream, cssStream)
      .pipe(concat('style.css'))
      .pipe(gulp.dest('./assets/dist/'));

  return mergedStream;
});
*/
/*
gulp.task("default", function () {
    var sassFiles = gulp.src('./src/style.sass')
    .pipe(sass())
    .pipe(concat("sass-style.css"));

    var cssFiles = gulp.src('assets/dist/style.css')
        .pipe(concat("style.css"));

    var lessCssFiles = gulp.src('./src/style.less')
        .pipe(less())
        .pipe(concat("less-files.css"));

    return merge( sassFiles, lessCssFiles,cssFiles)
        .pipe(concat("style.css"))
        .pipe(gulp.dest("/assets/dist/"));
});
*/